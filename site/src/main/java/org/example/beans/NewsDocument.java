
package org.example.beans;

import java.util.Calendar;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSetBean;

@Node(jcrType="customworkflow:newsdocument")
public class NewsDocument extends BaseDocument{

    public String getTitle() {
        return getProperty("customworkflow:title");
    }
    
    public String getSummary() {
        return getProperty("customworkflow:summary");
    }
    
    public HippoHtml getHtml(){
        return getHippoHtml("customworkflow:body");    
    }
    
    public Calendar getDate() {
        return getProperty("customworkflow:date");
    }

    /**
     * Get the imageset of the newspage
     *
     * @return the imageset of the newspage
     */
    public HippoGalleryImageSetBean getImage() {
        return getLinkedBean("customworkflow:image", HippoGalleryImageSetBean.class);
    }
    
}
