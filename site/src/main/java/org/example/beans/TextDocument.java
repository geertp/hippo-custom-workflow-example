
package org.example.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

@Node(jcrType="customworkflow:textdocument")
public class TextDocument extends BaseDocument{


    public String getTitle() {
        return getProperty("customworkflow:title");
    }
    
    public String getSummary() {
        return getProperty("customworkflow:summary");
    }
    
    public HippoHtml getHtml(){
        return getHippoHtml("customworkflow:body");    
    }
}
