package org.onehippo.examples.workflow;

import java.rmi.RemoteException;

import javax.jdo.annotations.DatastoreIdentity;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;

import org.hippoecm.repository.ext.WorkflowImpl;

@PersistenceCapable(identityType=IdentityType.DATASTORE,cacheable="false",detachable="true",table="documents")
@DatastoreIdentity(strategy=IdGeneratorStrategy.NATIVE)
public class NotificationWorkflowImpl extends WorkflowImpl implements NotificationWorkflow {

    private static final long serialVersionUID = 1L;

    public NotificationWorkflowImpl() throws RemoteException {
        super();
    }

    public void notify(String event, String documentId, String userId) {
        System.out.println("User " + userId + " performed action " + event + " on document " + documentId);
    }

}
