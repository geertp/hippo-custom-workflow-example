package org.onehippo.examples.workflow;

import org.hippoecm.repository.api.Workflow;

public interface NotificationWorkflow extends Workflow {

    public void notify(String event, String documentId, String userId);
}
