package org.onehippo.examples.workflow;

import java.rmi.RemoteException;

import javax.jcr.RepositoryException;
import javax.jdo.annotations.DatastoreIdentity;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;

import org.hippoecm.repository.api.MappingException;
import org.hippoecm.repository.api.Workflow;
import org.hippoecm.repository.api.WorkflowException;
import org.hippoecm.repository.reviewedactions.FullReviewedActionsWorkflow;
import org.hippoecm.repository.reviewedactions.FullReviewedActionsWorkflowImpl;

@PersistenceCapable(identityType=IdentityType.DATASTORE,cacheable="false",detachable="true", table="documents")
@DatastoreIdentity(strategy=IdGeneratorStrategy.NATIVE)
public class MyFullReviewedActionsWorkflowImpl extends FullReviewedActionsWorkflowImpl implements
        FullReviewedActionsWorkflow {

    private static final long serialVersionUID = 1L;

    private String documentId;

    public MyFullReviewedActionsWorkflowImpl() throws RemoteException {
        super();
    }

    @Override
    public void publish() throws WorkflowException, MappingException {
        super.publish();

        try {
            Workflow workflow = getWorkflowContext().getWorkflow("notification");

            if (workflow instanceof NotificationWorkflow) {
                NotificationWorkflow nwf = (NotificationWorkflow) workflow;

                nwf.notify("publish", documentId, userIdentity);
            }
        } catch (RepositoryException e) {
            throw new WorkflowException("Error calling notification workflow", e);
        }
    }

}
